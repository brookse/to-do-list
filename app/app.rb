require 'sinatra'
require 'sinatra/reloader' if development?
require 'sequel'

#DB = Sequel.connect('postgres://postgres:password@localhost:5432/tasks')
DB = Sequel.sqlite

DB.create_table :tasks do
	primary_key :id
	String :task
	Boolean :completed
end

tasks = DB[:tasks]

get '/' do
	@tasks = DB[:tasks] # get all tasks in database
	@title = 'All Tasks'
#	File.read(File.join('public', 'index.html'))
	erb :home
#	send_file('templates/home.html')
end

post '/' do
	puts tasks.inspect
	tasks.insert(:task => "hello", :completed =>false)
  #tasks.insert(:task => params[:content], :completed => false)
  redirect '/'
end

get '/:id' do
  @task = tasks(params[:id].to_i) # get task with matching id
  @title = "Edit task ##{params[:id].to_i}"
  erb :edit
end

put '/:id' do
#  t = tasks[params[:id].to_i] # get task with matching id
  tasks.where('id = ?', params[:id].to_i).update(:task => params[:content])
  redirect '/'
end

get '/:id/delete' do
  @note = tasks.where(:id=>params[:id].to_i)
  @title = "Confirm deletion of note ##{params[:id].to_i}"
  erb :delete
end

delete '/:id' do
# n = tasks.where(params[:id]
  tasks.where('id = ?', params[:id].to_i).delete
  redirect '/'
end

get '/:id/complete' do
  tasks.where('id = ?', params[:id].to_i).update(:completed => params[:complete] ? 0 : 1)
  redirect '/'
end